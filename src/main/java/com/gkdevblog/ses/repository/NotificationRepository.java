package com.gkdevblog.ses.repository;
import java.util.List;
 
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.gkdevblog.ses.dto.Notification;
 
public interface NotificationRepository extends CrudRepository<Notification, Long> {
     
    public List<Notification> findByStatus(int status);
     
//    @Query("SELECT e FROM use_notification e WHERE e.status >= :status")
//    public List<Notification> getNotificationByStatus(@Param("status") int status);
}