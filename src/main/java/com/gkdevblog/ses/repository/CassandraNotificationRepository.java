package com.gkdevblog.ses.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkdevblog.ses.dto.CassandraNotification;
 
@Repository
public interface CassandraNotificationRepository extends CrudRepository<CassandraNotification, Long> {
     
    public List<CassandraNotification> findByStatus(int status);
     
//    @Query("SELECT e FROM use_notification e WHERE e.status >= :status")
//    public List<Notification> getNotificationByStatus(@Param("status") int status);
}