package com.gkdevblog.ses.service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.ByteBuffer;
import java.util.List;
import java.util.Properties;

// JavaMail libraries. Download the JavaMail API 
// from https://javaee.github.io/javamail/
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;

// AWS SDK libraries. Download the AWS SDK for Java 
// from https://aws.amazon.com/sdk-for-java
import com.amazonaws.regions.Regions;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailService;
import com.amazonaws.services.simpleemail.AmazonSimpleEmailServiceClientBuilder;
import com.amazonaws.services.simpleemail.model.RawMessage;
import com.amazonaws.services.simpleemail.model.SendRawEmailRequest;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AmazonSESSample {

	// Replace sender@example.com with your "From" address.
	// This address must be verified with Amazon SES.
	// private static String SENDER = "Sender Name <sender@example.com>";

	// Replace recipient@example.com with a "To" address. If your account 
	// is still in the sandbox, this address must be verified.
	// private static String RECIPIENT = "recipient@example.com";

	// Specify a configuration set. If you do not want to use a configuration
	// set, comment the following variable, and the 
	// ConfigurationSetName=CONFIGURATION_SET argument below.
	// private static String CONFIGURATION_SET = "ConfigSet";

	// The subject line for the email.
	private static String SUBJECT = "Customer service contact info";

	// The full path to the file that will be attached to the email.
	// If you're using Windows, escape backslashes as shown in this variable.
	private static String ATTACHMENT = "avatar.png";

	// The email body for recipients with non-HTML email clients.
	private static String BODY_TEXT = "Hello,\r\n"
                                        + "Please see the attached file for a list "
                                        + "of customers to contact.";

	// The HTML body of the email.
	private static String BODY_HTML = "<html>"
                                        + "<head></head>"
                                        + "<body>"
                                        + "<h1>Hello!</h1>"
                                        + "<p>Please see the attached file for a "
                                        + "list of customers to contact.</p>"
                                        + "<img src='https://optionscircle.s3.ap-south-1.amazonaws.com/Option-circle-v-1x.png' alt='Red dot' />"
                                        + "</body>"
                                        + "</html>";
    private final AmazonSimpleEmailService emailService;
    private final String sender;
	  @Autowired
	    public AmazonSESSample(AmazonSimpleEmailService emailService,
	                         @Value("${email.sender}") String sender) {
	        this.emailService = emailService;
	        this.sender = sender;
	    }
	  
	 public void sendEmail(String emails) throws AddressException, MessagingException, IOException {
            	
    	Session session = Session.getDefaultInstance(new Properties());
    	 Message message = new MimeMessage(session);
    	 try {

             // Create a default MimeMessage object.
            

             // Set From: header field of the header.
             message.setFrom(new InternetAddress(sender));

             // Set To: header field of the header.
             message.setRecipients(Message.RecipientType.BCC,InternetAddress.parse(emails));

             // Set Subject: header field
             message.setSubject("Testing Subject");

             // This mail has 2 part, the BODY and the embedded image
             MimeMultipart multipart = new MimeMultipart("related");

             // first part (the html)
             BodyPart messageBodyPart = new MimeBodyPart();
             String htmlText = "<H1>Hello</H1><img src=\"cid:image\">";
             messageBodyPart.setContent(htmlText, "text/html");
             // add it
             multipart.addBodyPart(messageBodyPart);

             // second part (the image)
             messageBodyPart = new MimeBodyPart();
             DataSource fds = new FileDataSource("/home/kamlesh/avatar.png");

             messageBodyPart.setDataHandler(new DataHandler(fds));
             messageBodyPart.setHeader("Content-ID", "<image>");

             // add image to the multipart
             multipart.addBodyPart(messageBodyPart);

             // put everything together
             message.setContent(multipart);
            

            

          } catch (MessagingException e) {
             throw new RuntimeException(e);
          }

        // Try to send the email.
        try {
            System.out.println("Attempting to send an email through Amazon SES "
                              +"using the AWS SDK for Java...");
            // Instantiate an Amazon SES client, which will make the service 
            // call with the supplied AWS credentials.
            AmazonSimpleEmailService client = 
                    AmazonSimpleEmailServiceClientBuilder.standard()
                    // Replace US_WEST_2 with the AWS Region you're using for
                    // Amazon SES.
                    .withRegion(Regions.AP_SOUTH_1).build();
            
            // 	Print the raw email content on the console
            //  PrintStream out = System.out;
//            	message.writeTo(out);

            // Send the email.
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            message.writeTo(outputStream);
            RawMessage rawMessage = 
            		new RawMessage(ByteBuffer.wrap(outputStream.toByteArray()));

            SendRawEmailRequest rawEmailRequest = 
            		new SendRawEmailRequest(rawMessage);
            
            client.sendRawEmail(rawEmailRequest);
            System.out.println("Sent message successfully....");
        // Display an error if something goes wrong.
        } catch (Exception ex) {
          System.out.println("Email Failed");
            System.err.println("Error message: " + ex.getMessage());
            ex.printStackTrace();
        }
    }
	 
	 /*
     The resource URL is not working in the JAR
     If we try to access a file that is inside a JAR,
     It throws NoSuchFileException (linux), InvalidPathException (Windows)

     Resource URL Sample: file:java-io.jar!/json/file1.json
  */
 private File getFileFromResource(String fileName) throws URISyntaxException{

     ClassLoader classLoader = getClass().getClassLoader();
     URL resource = classLoader.getResource(fileName);
     if (resource == null) {
         throw new IllegalArgumentException("file not found! " + fileName);
     } else {

         // failed if files have whitespaces or special characters
         //return new File(resource.getFile());

         return new File(resource.toURI());
     }

 }
}