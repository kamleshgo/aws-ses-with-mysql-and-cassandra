package com.gkdevblog.ses.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkdevblog.ses.dto.EmailRequestDto;
import com.gkdevblog.ses.exception.AwsSesClientException;
import com.gkdevblog.ses.service.AmazonSESSample;
import com.gkdevblog.ses.service.AwsSesService;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/email")
public class EmailController {

    private final AwsSesService awsSesService;
    private final AmazonSESSample amazonSESSample;

    @Autowired
    public EmailController(AwsSesService awsSesService, AmazonSESSample amazonSESSample) {
        this.awsSesService = awsSesService;
        this.amazonSESSample = amazonSESSample;
    }

    @PostMapping(value = "", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEmail(@Valid @RequestBody EmailRequestDto emailRequestDto) {
        try {
        	amazonSESSample.sendEmail(emailRequestDto.getEmail());            
            return ResponseEntity.ok("Successfully Sent Email");
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Error occurred while sending email " + e);
        }
    }
}
