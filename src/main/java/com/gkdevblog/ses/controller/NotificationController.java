package com.gkdevblog.ses.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gkdevblog.ses.dto.CassandraNotification;
import com.gkdevblog.ses.dto.EmailRequestDto;
import com.gkdevblog.ses.dto.Notification;
import com.gkdevblog.ses.exception.AwsSesClientException;
import com.gkdevblog.ses.repository.CassandraNotificationRepository;
import com.gkdevblog.ses.repository.NotificationRepository;
import com.gkdevblog.ses.service.AmazonSESSample;
import com.gkdevblog.ses.service.AwsSesService;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1")
public class NotificationController {

    private final AwsSesService awsSesService;
    private final AmazonSESSample amazonSESSample;
    private final NotificationRepository repository;
    private final CassandraNotificationRepository cassandraRepository;
    
    
    @Autowired
    public NotificationController(AwsSesService awsSesService, AmazonSESSample amazonSESSample,NotificationRepository repository,CassandraNotificationRepository cassandraRepository) {
        this.awsSesService = awsSesService;
        this.amazonSESSample = amazonSESSample;
        this.repository=repository;
        this.cassandraRepository=cassandraRepository;
    }

    @PostMapping(value = "/notification", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveNotification(@RequestBody Notification notification) {
        try {
        	Calendar cal = Calendar.getInstance();            
        	notification.setCreatedDate(new Timestamp(cal.getTimeInMillis()));
        	notification.setUpdatedDate(new Timestamp(cal.getTimeInMillis()));
        	repository.save(notification);           
            return ResponseEntity.ok("Notification saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Error occurred " + e);
        }
    }
    
    @GetMapping(value = "/notification/get-notification-by-status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Notification> getNotificationByStatus(@PathVariable("status") final int status) {
        try {        	
        	List<Notification> notificationList=repository.findByStatus(status);        	
            return notificationList;
        } catch (Exception e) {
        	e.printStackTrace();
            return new ArrayList<Notification>();
        }
    }
    
    
    @GetMapping(value = "/notification/send-email-notification-by-status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEmailNotificationByStatus(@PathVariable("status") final int status) {
        try {        	
        	List<Notification> notificationList=repository.findByStatus(status);
        	if(notificationList.size()<1)
        		{
        			System.out.println("No emails pendding for notification");
        			return ResponseEntity.ok("No emails found");        			
        		}
        	String recipients = notificationList.stream()
                    .map(e -> e.updateStatus(2))
                    .collect( Collectors.joining( "," ) );
        		amazonSESSample.sendEmail(recipients);  
        		repository.saveAll(notificationList);
        		System.out.println("Email successfuly sent "+recipients);
            return ResponseEntity.ok("Successfully Sent Email");            
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResponseEntity.status(500).body("Error occurred while sending email " + e);
        }
    }
    
    @PostMapping(value = "/cnotification", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> saveCNotification(@RequestBody CassandraNotification notification) {
        try {
        	Calendar cal = Calendar.getInstance();            
        	notification.setCreatedDate(new Timestamp(cal.getTimeInMillis()));
        	notification.setUpdatedDate(new Timestamp(cal.getTimeInMillis()));
        	cassandraRepository.save(notification);           
            return ResponseEntity.ok("Notification saved successfully");
        } catch (Exception e) {
            return ResponseEntity.status(500).body("Error occurred " + e);
        }
    }
    
    
    @GetMapping(value = "/cnotification/get-notification-by-status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CassandraNotification> getCNotificationByStatus(@PathVariable("status") final int status) {
        try {        	
        	List<CassandraNotification> notificationList=cassandraRepository.findByStatus(status);   
        	//List<CassandraNotification> notificationList=(List<CassandraNotification>) cassandraRepository.findAll();   
            return notificationList;
        } catch (Exception e) {
        	e.printStackTrace();
            return new ArrayList<CassandraNotification>();
        }
    }
    
    
    @GetMapping(value = "/cnotification/send-email-notification-by-status/{status}", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<String> sendEmailCNotificationByStatus(@PathVariable("status") final int status) {
        try {        	
        	List<CassandraNotification> notificationList=cassandraRepository.findByStatus(status);
        	if(notificationList.size()<1)
        		{
        			System.out.println("No emails pendding for notification");
        			return ResponseEntity.ok("No emails found");        			
        		}
        	String recipients = notificationList.stream()
                    .map(e -> e.updateStatus(2))
                    .collect( Collectors.joining( "," ) );
        		amazonSESSample.sendEmail(recipients);  
        		cassandraRepository.saveAll(notificationList);
        		System.out.println("Email successfuly sent "+recipients);
            return ResponseEntity.ok("Successfully Sent Email");            
        } catch (Exception e) {
        	e.printStackTrace();
        	return ResponseEntity.status(500).body("Error occurred while sending email " + e);
        }
    }
}
