package com.gkdevblog.ses.dto;

import lombok.Getter;

import java.util.List;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

@Getter
public class EmailRequestDto {

	@NotEmpty(message = "Invalid Email address")
    private List<String> emails;
	
	@NotEmpty(message = "Invalid Email address")
    private String email;
	
    @NotEmpty(message = "Email body cannot be Null")
    private String body;
	
    

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public List<String> getEmails() {
		return emails;
	}
	public void setEmails(List<String> emails) {
		this.emails = emails;
	}
	public String getBody() {
		return body;
	}
	public void setBody(String body) {
		this.body = body;
	}
    
    
    
}
