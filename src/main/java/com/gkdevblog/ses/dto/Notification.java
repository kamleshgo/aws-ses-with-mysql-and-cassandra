package com.gkdevblog.ses.dto;

import lombok.Getter;


import java.sql.Timestamp;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Getter
@Entity
@Table(name = "use_notification")
public class Notification {
	
	public Notification() {
		super();
	}
	public Notification(int actionType, String email, int relatedId) {
		super();
		this.actionType = actionType;
		this.email = email;
		this.relatedId = relatedId;
	}

	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;   
	
	@Column(name = "created_date")	
	private Timestamp createdDate;
	
	@Column(name = "updated_date")	
	private Timestamp updatedDate;
    
	@Column(name = "action_type")	
	private int actionType;
 
	@Column(name = "email")	
	private String email;
	
	@Column(name = "related_id")	
	private int relatedId;	
	
	@Column(name = "status")	
	private int status;	// PENDING=1,PROCESSING=2,COMPLETE=3,ERROR=4

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getActionType() {
		return actionType;
	}
	public void setActionType(int actionType) {
		this.actionType = actionType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(int relatedId) {
		this.relatedId = relatedId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String updateStatus(int status) {
		this.status = status;
		return email;
	}
    
}
