package com.gkdevblog.ses.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import org.springframework.data.cassandra.core.mapping.Column;
import org.springframework.data.cassandra.core.mapping.PrimaryKey;
import org.springframework.data.cassandra.core.mapping.Table;
import java.sql.Timestamp;

@Table(value = "cassandra_notification")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class CassandraNotification {
	
	public CassandraNotification() {
		super();
	}
	public CassandraNotification(int actionType, String email, int relatedId) {
		super();
		this.actionType = actionType;
		this.email = email;
		this.relatedId = relatedId;
	}

	
	@PrimaryKey
    private Long id;   
	
	@Column("created_date")	
	private Timestamp createdDate;
	
	@Column("updated_date")	
	private Timestamp updatedDate;
    
	@Column("action_type")	
	private int actionType;
 
	@Column("email")	
	private String email;
	
	@Column("related_id")	
	private int relatedId;	
	
	@Column("status")	
	private int status;	// PENDING=1,PROCESSING=2,COMPLETE=3,ERROR=4

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public Timestamp getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Timestamp updatedDate) {
		this.updatedDate = updatedDate;
	}
	public int getActionType() {
		return actionType;
	}
	public void setActionType(int actionType) {
		this.actionType = actionType;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getRelatedId() {
		return relatedId;
	}

	public void setRelatedId(int relatedId) {
		this.relatedId = relatedId;
	}

	public int getStatus() {
		return status;
	}

	public void setStatus(int status) {
		this.status = status;
	}
	
	public String updateStatus(int status) {
		this.status = status;
		return email;
	}
    
}
