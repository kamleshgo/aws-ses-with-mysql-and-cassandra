**Set-up cassandra database using docker image and connect it with Spring boot application. **

Here I have already implemented Mysql connectivity using Spring boot JPA data and now in this documentation I will explain how to install, integrate and perform crus on cassandra daabase 

---

## Assumptions.

You already have install docker in your ubuntu base machie , OS version is 20.04
You arlready aware of spring boot started data cassandra API and CQL syntax and its result.

---

## Install cassandra via docker image

Next, follow the below steps to install cassandra using docker image

1. Pull docker image of the cassandra from docker hub
	```
	sudo docker pull cassandra:latest
	```
2. Run the docker image with container name and port number.
	```
	sudo docker run -d --name cassandra-node -p 9042:9042 cassandra
	```
3. RUN CQLSH TO INTERACT.
	```
	docker exec -it cassandra-node bash
	```
4. Create sample keyspace and some tables with data on CQLSH
	
	# Create a keyspace
	```
		CREATE KEYSPACE IF NOT EXISTS notification WITH REPLICATION = { 'class' : 'SimpleStrategy', 'replication_factor' : '1' };
	```		
	# Create a table
	```
		CREATE TABLE IF NOT EXISTS notification.cassandra_notification (
		  id bigint PRIMARY KEY,
		  created_date timestamp,
		  updated_date timestamp,
		  action_type int,
		  email text,
		  related_id int,
		  status int 
		);
	```		
		
	# Insert some data	
	```
	INSERT INTO notification.cassandra_notification(id,created_date,updated_date,action_type, email, related_id,status)
	VALUES (1,toTimeStamp(toDate(now())),toTimeStamp(toDate(now())),1,'email.address1@gmail.com',1,0);
	
	INSERT INTO notification.cassandra_notification(id,action_type, email,status)
	VALUES (2,1,'email.address2@gmail.com',0);
	```
		
---

## Now Let's move on Spring boot part

Add below dependancy in the pom.xml
	```
	<dependency>
		<groupId>org.springframework.boot</groupId>
		<artifactId>spring-boot-starter-data-cassandra</artifactId>
	</dependency>
	
	```
Add below configuration in the application.yml file
	```
	spring:
	  data:
		cassandra:
		  port: 9042 // port number 
		  contact-points: 127.0.0.1 // Here we can specifiy the host name where cassandra db is running
		  keyspace-name: notification // this is the keyspace name means database name.
		  username: cassandra 
		  password: cassandra
	```
Now implement enity model same as JPA data model but with cassandra supported annotation. in our case it is CassandraNotification

---

## Enetity class
	```
	package com.gkdevblog.ses.dto;
	
	import lombok.AllArgsConstructor;
	import lombok.Data;
	import lombok.NoArgsConstructor;
	import org.springframework.data.cassandra.core.mapping.Column;
	import org.springframework.data.cassandra.core.mapping.PrimaryKey;
	import org.springframework.data.cassandra.core.mapping.Table;
	import java.sql.Timestamp;

	@Table(value = "cassandra_notification")
	@Data
	@NoArgsConstructor
	@AllArgsConstructor
	public class CassandraNotification {

		public CassandraNotification() {
			super();
		}
		public CassandraNotification(int actionType, String email, int relatedId) {
			super();
			this.actionType = actionType;
			this.email = email;
			this.relatedId = relatedId;
		}


		@PrimaryKey
		private Long id;   

		@Column("created_date")	
		private Timestamp createdDate;

		@Column("updated_date")	
		private Timestamp updatedDate;

		@Column("action_type")	
		private int actionType;

		@Column("email")	
		private String email;

		@Column("related_id")	
		private int relatedId;	

		@Column("status")	
		private int status;	// PENDING=1,PROCESSING=2,COMPLETE=3,ERROR=4

		public Long getId() {
			return id;
		}

		public void setId(Long id) {
			this.id = id;
		}


		public Timestamp getCreatedDate() {
			return createdDate;
		}
		public void setCreatedDate(Timestamp createdDate) {
			this.createdDate = createdDate;
		}
		public Timestamp getUpdatedDate() {
			return updatedDate;
		}
		public void setUpdatedDate(Timestamp updatedDate) {
			this.updatedDate = updatedDate;
		}
		public int getActionType() {
			return actionType;
		}
		public void setActionType(int actionType) {
			this.actionType = actionType;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public int getRelatedId() {
			return relatedId;
		}

		public void setRelatedId(int relatedId) {
			this.relatedId = relatedId;
		}

		public int getStatus() {
			return status;
		}
		public void setStatus(int status) {
			this.status = status;
		}

		public String updateStatus(int status) {
			this.status = status;
			return email;
		}    
	}
	```

Now let's  implements the crud repo for the above class with casandra support. it is almost same as spring data JPA but packages paths are differents.

```
package com.gkdevblog.ses.repository;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.gkdevblog.ses.dto.CassandraNotification;
 
@Repository
public interface CassandraNotificationRepository extends CrudRepository<CassandraNotification, Long> {
     
    public List<CassandraNotification> findByStatus(int status);
     
//    @Query("SELECT e FROM use_notification e WHERE e.status >= :status")
//    public List<Notification> getNotificationByStatus(@Param("status") int status);
}
```



##Ref.

	https://www.youtube.com/watch?v=74m6szWIEtM
	
	https://www.youtube.com/watch?v=JmwcGJP3pzM
	
	https://www.digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04